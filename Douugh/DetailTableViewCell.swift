//
//  DetailTableViewCell.swift
//  Douugh
//
//  Created by Emad A. on 3/9/18.
//  Copyright © 2018 Emad A. All rights reserved.
//

import UIKit

final class DetailTableViewCell: UITableViewCell {
    
    class var identifier: String {
        return "DetailCell"
    }
    
    // MARK: -
    
    private let keyLabel: UILabel = {
        let label = UILabel()
        label.font = .preferredFont(forTextStyle: .caption1)
        label.snp.makeConstraints { $0.width.equalTo(70) }
        label.textAlignment = .right
        label.textColor = .gray
        return label
    }()
    
    private let valueLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    // MARK: -
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        contentView.layoutMargins = UIEdgeInsets(top: 16, left: 12, bottom: 16, right: 12)
        
        let stackView = UIStackView(arrangedSubviews: [keyLabel, valueLabel])
        stackView.axis = .horizontal
        stackView.spacing = 10
        contentView.addSubview(stackView)
        stackView.snp.makeConstraints { $0.edges.equalTo(contentView.layoutMarginsGuide) }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(key: String?, value: String?) {
        keyLabel.text = key
        valueLabel.text = value
    }
    
}

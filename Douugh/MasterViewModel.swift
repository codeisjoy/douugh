//
//  MasterViewModel.swift
//  Douugh
//
//  Created by Emad A. on 3/9/18.
//  Copyright © 2018 Emad A. All rights reserved.
//

import UIKit

enum UsersOrder: Int {
    case firstName = 0
    case lastName  = 1
    case id        = 2
}

final class MasterViewModel {
    
    private(set) var users = [User]() {
        didSet { DispatchQueue.main.async { [unowned self] in self.onUserUpdate(self.users) } }
    }
    
    // MARK: -
    
    private let url = "https://gist.githubusercontent.com/douughios/f3c382f543a303984c72abfc1d930af8/raw/5e6745333061fa010c64753dc7a80b3354ae324e/test-users.json"
    
    private let session = URLSession(configuration: .default)
    
    private(set) var order: UsersOrder = .firstName
    
    private let onUserUpdate: ([User]) -> Void
    
    // MARK: -
    
    init(onUserUpdate: @escaping ([User]) -> Void) {
        self.onUserUpdate = onUserUpdate
    }
    
    func loadUsers() {
        guard let url = URL(string: self.url) else {
            return
        }
        session.dataTask(with: url) { [weak self] data, response, error in
            guard let data = data else {
                self?.users = [User]()
                return
            }
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            guard let users = try? decoder.decode([User].self, from: data),
                let ordered = self?.ordered(users: users) else {
                self?.users = []
                return
            }
            self?.users = ordered
        }.resume()
    }
    
    func user(at indexPath: IndexPath) -> User? {
        let index = indexPath.row
        guard users.indices.contains(index) else {
            return nil
        }
        return users[index]
    }
    
    func changeUserOrdered(to order: UsersOrder) {
        self.order = order
        users = ordered(users: users)
    }
    
    private func ordered(users: [User]) -> [User] {
        let order = self.order
        return users.sorted(by: { lhs, rhs -> Bool in
            switch order {
            case .firstName: return lhs.firstName < rhs.firstName
            case .lastName : return lhs.lastName  < rhs.lastName
            case .id       : return lhs.id        < rhs.id
            }
        })
    }
    
}

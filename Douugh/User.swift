//
//  User.swift
//  Douugh
//
//  Created by Emad A. on 3/9/18.
//  Copyright © 2018 Emad A. All rights reserved.
//

import Foundation

struct User: Decodable {
    
    let id       : Int
    let firstName: String
    let lastName : String
    let email    : String
    
    var fullName: String {
        return [firstName, lastName].joined(separator: " ")
    }
    
}

//
//  AppDelegate.swift
//  Douugh
//
//  Created by Emad A. on 3/9/18.
//  Copyright © 2018 Emad A. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = .white
        
        let viewController = UISplitViewController()
        viewController.preferredDisplayMode = .allVisible
        viewController.view.backgroundColor = .groupTableViewBackground
        viewController.viewControllers = [UINavigationController(rootViewController: MasterViewController())]
        window.rootViewController = viewController
        
        window.makeKeyAndVisible()
        self.window = window
        
        return true
    }

}


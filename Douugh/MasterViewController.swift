//
//  MasterViewController.swift
//  Douugh
//
//  Created by Emad A. on 3/9/18.
//  Copyright © 2018 Emad A. All rights reserved.
//

import UIKit

final class MasterViewController: UIViewController {
    
    private(set) lazy var viewModel: MasterViewModel = {
        return MasterViewModel(onUserUpdate: { [weak self] users in
            self?.tableView.refreshControl?.endRefreshing()
            self?.tableView.reloadData()
        })
    }()
    
    private let orderControl = UISegmentedControl(items: ["First Name", "Last Name", "ID"])
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.estimatedRowHeight = 45
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(MasterTableViewCell.self, forCellReuseIdentifier: MasterTableViewCell.identifier)
        return tableView
    }()
    
    // MARK: -
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { $0.edges.equalToSuperview() }
        
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        orderControl.selectedSegmentIndex = viewModel.order.rawValue
        orderControl.addTarget(self, action: #selector(needsUpdateUsersOrder), for: .valueChanged)
        navigationItem.titleView = orderControl
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(needsRefreshUsers), for: .valueChanged)
        
        tableView.refreshControl = refreshControl
        
        refreshControl.beginRefreshing()
        refreshControl.sendActions(for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selected = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selected, animated: true)
        }
    }
    
}

extension MasterViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MasterTableViewCell.identifier, for: indexPath)
        if let cell = cell as? MasterTableViewCell, let user = viewModel.user(at: indexPath) {
            cell.configure(with: user)
        }
        return cell
    }
    
}

extension MasterViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let user = viewModel.user(at: indexPath) else {
            return
        }
        let detailViewController = DetailViewController(user: user)
        showDetailViewController(UINavigationController(rootViewController: detailViewController), sender: self)
    }
    
}

private extension MasterViewController {
    
    @objc func needsRefreshUsers() {
        guard tableView.refreshControl?.isRefreshing == true else {
            return
        }
        viewModel.loadUsers()
    }
    
    @objc func needsUpdateUsersOrder() {
        guard let order = UsersOrder(rawValue: orderControl.selectedSegmentIndex) else {
            return
        }
        viewModel.changeUserOrdered(to: order)
    }
    
}

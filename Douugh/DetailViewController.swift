//
//  DetailViewController.swift
//  Douugh
//
//  Created by Emad A. on 3/9/18.
//  Copyright © 2018 Emad A. All rights reserved.
//

import UIKit

private enum UserDetail: Int {
    case fullName = 0
    case email    = 1
    case id       = 2
    
    var key: String {
        switch self {
        case .fullName: return "Full Name"
        case .email   : return "Email"
        case .id      : return "ID"
        }
    }
}

final class DetailViewController: UIViewController {
    
    private let user: User
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.allowsSelection = false
        tableView.register(DetailTableViewCell.self, forCellReuseIdentifier: DetailTableViewCell.identifier)
        return tableView
    }()
    
    // MARK: -
    
    init(user: User) {
        self.user = user
        super.init(nibName: nil, bundle: nil)
        self.title = "User Detail"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { $0.edges.equalToSuperview() }
        
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
    }

}

extension DetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DetailTableViewCell.identifier, for: indexPath)
        if let detail = UserDetail(rawValue: indexPath.row) {
            var value: String?
            switch detail {
            case .fullName: value = user.fullName
            case .email   : value = user.email
            case .id      : value = user.id.description
            }
            (cell as? DetailTableViewCell)?.configure(key: detail.key, value: value)
        }
        return cell
    }
    
}

//
//  MasterTableViewCell.swift
//  Douugh
//
//  Created by Emad A. on 3/9/18.
//  Copyright © 2018 Emad A. All rights reserved.
//

import UIKit

final class MasterTableViewCell: UITableViewCell {

    class var identifier: String {
        return "MasterCell"
    }
    
    // MARK: -
    
    private let fullNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .darkText
        label.font = .preferredFont(forTextStyle: .headline)
        return label
    }()

    private let emailLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.numberOfLines = 0
        label.font = .preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    // MARK: -
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        accessoryType = .disclosureIndicator
        
        contentView.addSubview(fullNameLabel)
        fullNameLabel.snp.makeConstraints {
            $0.leading.top.trailing.equalTo(contentView.layoutMarginsGuide)
        }
        
        contentView.addSubview(emailLabel)
        emailLabel.snp.makeConstraints {
            $0.top.equalTo(fullNameLabel.snp.bottom)
            $0.leading.bottom.trailing.equalTo(contentView.layoutMarginsGuide)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with user: User) {
        fullNameLabel.text = user.fullName
        emailLabel.text = user.email
    }
    
}
